package pl.sdacademy.threads;

public class Counter {
    private int value;

    public int getValue() {
        return value;
    }

    public void incrementValue() {
        // SEKCJA KRYTYCZNA:
        // fragment kodu programu, w którym korzysta się z zasobu dzielonego,
        // a co za tym idzie w danej chwili może być wykorzystywany przez co najwyżej jeden wątek
        synchronized (this) {
            value++;
        }
    }

}
