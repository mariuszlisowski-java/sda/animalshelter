package pl.sdacademy.threads;

import lombok.Builder;
import lombok.ToString;

import java.time.Duration;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.UUID;

public class CustomMessageSystem {
    private static final Queue<String> messageQueue = new LinkedList<>();

    public static void main(String... args) {
        Thread messageProducer = new Thread(() -> {
            while (true) {
                System.out.println("Sending the message...");
                Random random = new Random();
                try {
                    Thread.sleep(Duration.ofSeconds(random.nextInt(6)).toMillis());
                } catch (InterruptedException exception) {
                    exception.printStackTrace();
                }

                synchronized (messageQueue) {
                    messageQueue.add(
                            Message.builder()
                                    .id(UUID.randomUUID().toString())
                                    .description("message")
                                    .build().toString()
                    );
                    messageQueue.notify(); // notify
                }
            }
        });

        Thread messageConsumer = new Thread(() -> {
            while (true) {
                System.out.println("Waiting for the message...");
                String message;
                synchronized (messageQueue) {
                    if (messageQueue.isEmpty()) {
                        try {
                            messageQueue.wait(); // wait for a thread to notify
                        } catch (InterruptedException exception) {
                            exception.printStackTrace();
                        }
                    } else {
                        message = messageQueue.poll();
                        System.out.println(message + " " + messageQueue.size());
                        // delay when message caught (verbose)
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException exception) {
                            exception.printStackTrace();
                        }
                    }
                }
            }
        });

        messageProducer.start();
        messageConsumer.start();
    }


}

@Builder
@ToString
class Message {
    String id;
    String description;
}
