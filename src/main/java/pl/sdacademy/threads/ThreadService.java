package pl.sdacademy.threads;

public class ThreadService {
    public static void main(String[] args) {
        Counter counter = new Counter();

        Runnable runnable = () -> {
            System.out.println(Thread.currentThread().getName() + " started");
            for (int i = 0; i < 100_000; i++) {
                counter.incrementValue();
            }
            System.out.println(Thread.currentThread().getName() + " stopped");
        };

        Thread threadA = new Thread(runnable);
        Thread threadB = new Thread(runnable);
        Thread threadC = new Thread(runnable);

        threadA.start();
        threadB.start();
        threadC.start();

//        while (threadA.isAlive() ||
//               threadB.isAlive() ||
//               threadC.isAlive()) {}

        try {
            threadA.join();
            threadB.join();
            threadC.join();
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }


        System.out.println(counter.getValue());
    }

}