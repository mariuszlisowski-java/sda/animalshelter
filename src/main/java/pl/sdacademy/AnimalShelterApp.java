package pl.sdacademy;

import pl.sdacademy.api.DogCreatorFunction;
import pl.sdacademy.api.ICatAndDogFunctional;
import pl.sdacademy.api.ICatAndDogFunctionalReturn;
import pl.sdacademy.dto.AnimalReportRequest;
import pl.sdacademy.model.Animal;
import pl.sdacademy.model.Cat;
import pl.sdacademy.model.Dog;
import pl.sdacademy.repo.AnimalRepo;
import pl.sdacademy.threads.MyThread;
import pl.sdacademy.threads.MyThreadRunnable;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class AnimalShelterApp {
    public static final String CODE = "CODE: ";

    public static void main(String[] args) {
        // COMMON:
        Dog bulldog = new Dog(
                UUID.randomUUID().toString(),
                "Rex",
                25,
                5
        );
        Dog shepard = new Dog(
                UUID.randomUUID().toString(),
                "Nero",
                45,
                3
        );

        Cat kitty = new Cat(
                UUID.randomUUID().toString(),
                "Kitty",
                3,
                2,
                false
        );

        AnimalRepo.insertAnimal(shepard);

        Collection<Animal> animals = AnimalRepo.showAnimals();
        for (Animal animal : animals) {
            System.out.println(animal);
        }

        Collection<String> chips = AnimalRepo.getAnimalChipsWithAge3();
        for (String chip : chips) {
            System.out.println("chip: " + chip);
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////
        // FUNCTIONAL INTERFACE:

        List<String> names = Arrays.asList("Ann", "Alice", "Peter", "Tom");
        names.stream().forEach(name -> System.out.println(name));

        Stream<String> namesStream = names.stream();
        Consumer<String> stringConsumer = name -> System.out.println(name);
        ICatAndDogFunctional catAndDogShow = (cat, dog) -> System.out.println(dog.toString() + cat.toString());
        catAndDogShow.apply(
                new Cat(UUID.randomUUID().toString(), "Mruk", 3, 1, false),
                bulldog);

        ICatAndDogFunctional catAndDogWeight =
                (cat, dog) -> System.out.println("dog: " + dog.getWeight() + " cat: " + cat.getWeight());

        catAndDogWeight.apply(
                new Cat(UUID.randomUUID().toString(), "Mruk", 3, 1, false),
                bulldog);

        // functional interface with return type
        ICatAndDogFunctionalReturn iCatAndDogFunctionalReturn =
                (cat, dog) -> "any string to return";
        System.out.println(iCatAndDogFunctionalReturn.apply(kitty, shepard));

        iCatAndDogFunctionalReturn.apply(
                new Cat(UUID.randomUUID().toString(), "Mruk", 3, 1, false),
                bulldog);

        ICatAndDogFunctionalReturn func =
                (cat, dog) -> {
                    String catChip = cat.chipCode;
                    String dogChip = dog.chipCode;

                    return catChip.concat("-").concat(dogChip);
                };
        String output = func.apply(kitty, shepard);
        System.out.println(output);

        // function
        Function<String, Dog> dogByNameFactory = name -> new Dog(
                UUID.randomUUID().toString(),
                name,
                45,
                3
        );
        Dog doggy = dogByNameFactory.apply("Nero");
        System.out.println(doggy.toString());

        DogCreatorFunction dogCreatorFunction = (age, name) -> new Dog(
                UUID.randomUUID().toString(),
                name,
                age
        );

        List<Dog> dogs = Arrays.asList(
                dogCreatorFunction.create(4, "DoggyA"),
                dogCreatorFunction.create(2, "DoggyB"),
                dogCreatorFunction.create(3, "DoggyC")
        );

        // without predicate
        dogs.stream()
                .forEach(dog -> System.out.println(dog.name));
        System.out.println();

        // with predicate
        Predicate<Dog> dogPredicate =
                dog -> Integer.valueOf(2).compareTo(dog.age) <= 0; // check Predicate conditions
        dogs.stream()
                .filter(dogPredicate)
                .forEach(dog -> System.out.println(dog.name));

        // REFLECTION:
        AnimalReportRequest request = AnimalReportRequest.builder()
                .animalChipCode("ADA-35678")
                .dateCode("DAT-09-11-2020")
                .animalName("Rex")
                .shelterCode("SHELTER-001").build();

        System.out.println(addCodeStringToParams(request).toString());

        //////////////////////////////////////////////////////////////////////////////////////////////////////
        // TREADS:

        // extends thread
        Thread threadA = new MyThread();
        System.out.println(threadA.isAlive());
        threadA.start();
        System.out.println(threadA.isAlive());

        // implement runnable
        MyThreadRunnable myThreadRunnable = new MyThreadRunnable();
        Thread threadB = new Thread(myThreadRunnable);
        threadB.start();

        // implement thread with runnable
        Thread firstThread = new Thread(
                () -> {
                    for (int i = 0; i < 10; i++) {
                        System.out.println("Thread: " + i);
                    }
                }
        );
        firstThread.start();

        Thread secondThread = new Thread(
                () -> {
                    for (int i = 0; i < 10; i++) {
                        System.out.println("Thread: " + i);
                    }
                }
        );
        secondThread.start();

        for (int i = 0; i < 10; i++) {
            System.out.println("Main: " + i);
        }


    } // main

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    // REFLECTION METHODS:

    private static Object addCodeStringToParams(Object object) {
        Class<?> myClass = object.getClass();

        for (Method method : myClass.getDeclaredMethods()) {
            if (method.getName().contains("set")
                    && method.getName().contains("Code")) {
                try {
                    Method getter = myClass.getDeclaredMethod("get" + method.getName().substring(3));
                    String codeValue = (String) getter.invoke(object);
                    method.invoke(object, CODE + codeValue);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return object;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    // FUNCTIONAL INTERFACE METHODS:

    // regular method (instead of lambda)
    public String foo(Cat cat, Dog dog) {
        String catChip = cat.chipCode;
        String dogChip = dog.chipCode;

        return catChip.concat("-").concat(dogChip);
    }

}
