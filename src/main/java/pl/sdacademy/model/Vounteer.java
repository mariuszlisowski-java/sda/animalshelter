package pl.sdacademy.model;

import java.util.*;

public class Vounteer {
    String name;
    Set<Animal> animalsToCare;
    Queue<CareEvent> careEvents;

    public Vounteer(String name) {
        this.name = name;
        this.animalsToCare = new TreeSet<>();
        this.careEvents = new PriorityQueue<>();
    }

    void careAnimal(Animal animal) {
        animalsToCare.add(animal);
    }

    boolean ifAnimalIsCared(String name) {
        boolean isAnimalCared = false;
        for (Animal animal : animalsToCare) {
            if (name != null & name.equals(animal.name)) {
                isAnimalCared = true;
            }
        }
        return isAnimalCared;
    }

    private void makeCareEvent() {
        CareEvent careEvent = careEvents.poll();
        if (careEvent != null) {
            System.out.println(careEvent.description);
        }
    }
}
