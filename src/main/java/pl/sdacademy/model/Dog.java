package pl.sdacademy.model;

public class Dog extends Animal {
    public Dog(String chipCode, String name, Integer weight, Integer age) {
        super(chipCode, name, weight, age);
    }

    public Dog(String chipCode, String name, int age) {
        super(chipCode, name, age);
    }
}
