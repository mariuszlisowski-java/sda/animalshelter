package pl.sdacademy.model;

import java.time.LocalDate;

public class CareEvent {
    String name;
    LocalDate startDate;
    LocalDate endDate;
    String description;
}
