package pl.sdacademy.model;

public class Cat extends Animal {
    boolean isUnsafe;

    public Cat(String chipcode, String name, Integer weight, Integer age, boolean isUnsafe) {
        super(chipcode, name, weight, age);
        this.isUnsafe = isUnsafe;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "isUnsafe=" + isUnsafe +
                ", chipCode='" + chipCode + '\'' +
                ", name='" + name + '\'' +
                ", weight=" + weight +
                ", age=" + age +
                '}';
    }
}
