package pl.sdacademy.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Objects;

//@AllArgsConstructor
//@NoArgsConstructor
//@Setter
//@Slf4j  // logger
public abstract class Animal {
    public String chipCode;
    public String name;
    public Integer weight;
    public Integer age;

    public Animal(String chipCode, String name, Integer weight, Integer age) {
        this.chipCode = chipCode;
        this.name = name;
        this.weight = weight;
        this.age = age;
    }

    public Animal(String chipCode, String name, int age) {
        this.chipCode = chipCode;
        this.name = name;
        this.age = age;
    }

    public Integer getWeight() {
        return weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return Objects.equals(chipCode, animal.chipCode) &&
                Objects.equals(name, animal.name) &&
                Objects.equals(weight, animal.weight) &&
                Objects.equals(age, animal.age);
    }

    @Override
    public int hashCode() {
        return Objects.hash(chipCode, name, weight, age);
    }

}
