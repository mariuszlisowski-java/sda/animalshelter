package pl.sdacademy.api;

import pl.sdacademy.model.Cat;
import pl.sdacademy.model.Dog;

// consumer interface
@FunctionalInterface
public interface ICatAndDogFunctional {
    void apply(Cat cat, Dog dog);
}
