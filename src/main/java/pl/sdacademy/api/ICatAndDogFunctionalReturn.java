package pl.sdacademy.api;

import pl.sdacademy.model.Cat;
import pl.sdacademy.model.Dog;

@FunctionalInterface
public interface ICatAndDogFunctionalReturn {
    String apply(Cat cat, Dog dog);
}
