package pl.sdacademy.api;

import pl.sdacademy.model.Dog;

@FunctionalInterface
public interface DogCreatorFunction {
    Dog create(int age, String name);
}
