package pl.sdacademy.repo;

import pl.sdacademy.model.Animal;

import java.util.*;

public class AnimalRepo {
    private static Map<String, Animal> animals = new HashMap<>();

    public static void insertAnimal(Animal animal) {
        animals.put(animal.chipCode, animal);
    }

    public static Collection<Animal> showAnimals() {
        return animals.values();
    }

    public static Animal getAnimalByChipCode(String chipCode) {
        return animals.get(chipCode);
    }

    public static Collection<String> getAnimalChipsWithAge3() {
        Collection<String> animalList = new ArrayList<>();
        for (Animal animal : animals.values()) {
            if (Integer.valueOf(3).compareTo(animal.age) == 0) {
                animalList.add(animal.chipCode);
            }
        }
        return animalList;
    }

    public static void makeMagicByChipCode(String chip) {
        Animal animal = animals.get(chip);
        System.out.println("Magic! : " + animal.name);
    }
}
