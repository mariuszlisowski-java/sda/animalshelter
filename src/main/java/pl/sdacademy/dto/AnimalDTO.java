package pl.sdacademy.dto;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Builder
@Data
@ToString
public class AnimalDTO {
    String chip;
    String name;
}
