package pl.sdacademy.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class AnimalReportRequest {
    String animalChipCode;
    String dateCode;
    String shelterCode;
    String animalName;

    public String doMagic(){
        return "Magic!";
    }

    @Override
    public String toString() {
        return "AnimalReportRequest{" +
                "\nanimalChipCode='" + animalChipCode + '\'' +
                ",\n dateCode='" + dateCode + '\'' +
                ",\n shelterCode='" + shelterCode + '\'' +
                ",\n animalName='" + animalName + '\'' +
                '}';
    }
}
